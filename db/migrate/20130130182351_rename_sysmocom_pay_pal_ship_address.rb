class RenameSysmocomPayPalShipAddress < ActiveRecord::Migration
  def up
    rename_column :spree_orders, :paypal_ship_address_id, :sysmocom_paypal_ship_address_id
  end

  def down
    rename_column :spree_orders, :sysmocom_paypal_ship_address_id, :paypal_ship_address_id
  end
end
