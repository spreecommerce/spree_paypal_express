class OrderPayPalAddressField < ActiveRecord::Migration
  def self.up
    change_table :orders do |t|
      t.integer :paypal_ship_address_id
    end
  end

  def self.down
    change_table :orers do |t|
      t.remove :paypal_ship_address_id
    end
  end
end
